﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Game : MonoBehaviour
{
    public Text startWorkText; //Текст клика для начала работы
    public Text nameWorkText; //Текст названия где работаешь 
    public Text detailsText; //Текст вывода произведенных деталей
    public Text workText; //Текст главного клика
    public Text moneyText; //Текст вывода деняг

    public Text timeText;

    public int sizeWorkDay = 160; // Размер рабочего дня. Возьмем в начале хотя бы такое число, чтобы знать, что цикл остановится 
    public int scoreClick = 1; // Очки за один клик
    public int details = 0; // Детали
    public int money = 0; // Деньги 
    public int globalTime = 0; //Глобальное время 
    public int workTime = 0; // Рабочее время 
    public int priceDetails = 50; //Цена за одну деталь
    public int timeVelocity = 5; //Коефицент ускорения времени


    public void OnClick () //Метод суммирования "очки за один клик" к "деталям"
    {
        if (workTime > 0)
        {
            details += scoreClick;
            detailsText.text = "Сделано деталей:" + details;

        }
        else
        {
            workText.text = "Начните рабочий день!";
        }
    }

    IEnumerator Time ()                                         // Иенумератор. Постоянно прибавляет к рабочему и глобальному временам "коеф ускорения времени"
    {
        while (workTime < sizeWorkDay)
        {
            workTime += timeVelocity;
            globalTime += timeVelocity;
           // IntupTime();
            yield return new WaitForSeconds(1);
        }
        CaclulateMoney();
        timeText.text = "Вы закончили рабочий день.";
        startWorkText.text = "Начните следущий рабочий день.";
        workTime = 0;                                           // В конце просто обновляем рабочее время и детали
        details = 0;
    }

    public void StartWork()                             // Начало рабочего дня (времени)
    {
        if (workTime < 1)
        {
            StartCoroutine(Time());
            startWorkText.text = "Вы уже работаете";
            workText.text = "Работать!";
        }
        else
        {
            startWorkText.text = "Вы уже работаете";
        }
    }

    public void CaclulateMoney ()                   //Подсчет денег за рабочий день
    {
        money = money + details * priceDetails;
        moneyText.text = "Вы заработали: " + money;
    }

    public class Save                   //Метод сохранения игры (Вообще не реализован)
    {
        public int money;

    }

/* Попытка реализации вывода времени посекундно
    public void IntupTime()
    {
        while (workTime > 1 && workTime < 160)
        {
            timeText.text = "Вы работаете: " + timer.hours + " часов " + timer.minuts + " минут " + timer.seconds + " секунд.";
            if (globalTime < 60)
            {
                timer.seconds++;
            }
            else if (timer.seconds < 60)
            {
                timer.minuts++;
                timer.seconds = 0;
            }
            else if (timer.minuts < 60)
            {
                timer.hours++;
                timer.minuts = 0;
            }
            else if (timer.hours < 8)
            {
                break;
            }
        }
    }
*/
    public void Update()
    {
        timeText.text = "Вы работаете: " + workTime;
    }

}
